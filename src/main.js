import { SolutionModule } from './solution/solution.module';
require('./style.css');

angular.element(() => {
    const app = angular.module('app', [ SolutionModule.name ]);
    angular.bootstrap(document, [ app.name ]);
});
